---
layout: markdown_page
title: "Community Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# Welcome to the Community Marketing Handbook  

The Community Marketing organization includes Community Advocacy and Committer Program.

Community Marketing Handbooks:  

- [Community Advocacy](/handbook/marketing/community-marketing/community-advocacy/)
- [Committer Program](/handbook/marketing/community-marketing/committer-program)
