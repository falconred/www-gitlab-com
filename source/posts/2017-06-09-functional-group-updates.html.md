---
title: "GitLab's Functional Group Updates - June 5th - June 9th" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: company
description: "The Functional Groups at GitLab give an update on what they've been working on"
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/functional-group-updates/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

## Recordings

All of the updates are recorded using [Zoom](https://zoom.us) at the time of the call. All the recordings will be uploaded to our YouTube account and made public, with the exception of the Sales and Finance updates.

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Channel Team

[Presentation slides](https://docs.google.com/presentation/d/1qt0y_RwMF1GBAHiUSVdl1SzwIxwk1vyNHEznx_Cia5o/edit#slide=id.p)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/eH-GuoqlweM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Frontend Team

[Presentation slides](https://docs.google.com/presentation/d/12_H0jxd2RDjIrwOi9A0vw6dblUTaAe9QOOQosFAPXTE/edit#slide=id.g1dbb49e349_0_5)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/bXjfn7tEgJ8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Prometheus Team

[Presentation slides](https://docs.google.com/presentation/d/1x8jNX0VbkMsurp9iveHTaYByUBQPzHrldf_5JNEMIFM/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/xSIwVHu1J3Q" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Engineering Team

[Presentation slides](https://docs.google.com/presentation/d/13XU9dzhvpHJ625VnKTRv4unldTdPuO3_C4MmbZ28mdc/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/dRSl8UlmdU4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### UX Team

[Presentation slides](https://docs.google.com/presentation/d/1jeQYeVmWk6uYU8dSwBMOyPY2HcAQNX-cx9PQOdgaD70/edit#slide=id.g1d2e18b374_0_555)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/7N0ZKRmgLJQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----


Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
